<?php

    // Principais Dados do Cliente
    $nome_empresa = "Apliflow";
    $emailContato = "comercial@movmed.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "Apliflow",
            "rua" => "R. Arapari, 223",
            "bairro" => "São Geraldo",
            "cidade" => "Belo Horizonte",
            "estado" => "Minas Gerais",
            "uf" => "MG",
            "cep" => "31050-540",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "31",
            "telefone" => "3487-1600",
            "whatsapp" => "98689-3166",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.apliflow.com.br/",
        // URL online
        "https://www.informacoes.apliflow.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Calibração de medidores de densidade",
"Calibração de medidores de pressão",
"Calibração de medidores de temperatura",
"Calibração de medidores de vazão",
"Calibração de transmissores de pressão",
"Calibração de transmissores hidrostático",
"Calibração de válvulas de Segurança",
"Calibração em Redutores de pressão",
"Calibração em Válvulas de controle",
"Calibração em Válvulas de Retenção",
"Calibração em Válvulas de segurança",
"Fabricação de equipamentos",
"Limpeza e desobstrução dos medidores",
"Locação de Chave de Fluxo",
"Locação de Detector de óleo em água",
"Locação de Equipamentos industriais",
"Locação de Medidor de temperatura",
"Locação de Medidores de Nível",
"Locação de Medidores de Vazão",
"Locação de Transmissor de pressão",
"Manutenção de medidores de densidade",
"Manutenção de medidores de pressão",
"Manutenção de medidores de temperatura",
"Manutenção de medidores de vazão",
"Manutenção de purgadores",
"Manutenção de Transmissores de pressão",
"Manutenção em Redutores de pressão",
"Manutenção em transmissores de pressão",
"Manutenção em Transmissores hidrostático",
"Manutenção em Válvulas de bloqueio",
"Manutenção em Válvulas de controle",
"Manutenção em Válvulas de Retenção",
"Manutenção em Válvulas de retenção preço",
"Manutenção em Válvulas de segurança",
"Reparo em medidores coriolis",
"Reparo em medidores magnéticos",
"Reparos em medidores vórtex",
"Restauração dos invólucros dos medidores",
"Venda de Equipamentos Industriais"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */