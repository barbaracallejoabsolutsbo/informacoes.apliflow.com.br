<header itemscope itemtype="http://schema.org/Organization">

    
    
    <div class="topo">
        <div class="container">
            <div class="col-md-10">
                <div class="tel">
                    <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
                        <i class="fas fa-phone-alt"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span>
                    </a> 
                    |
                    <a title="whatsApp" href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">
                        <i class="fab fa-whatsapp"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span>
                    </a> 
                    |
                    <a title="E-mail" href="mailto:<?php echo $emailContato; ?>?CC=<?php echo $emailAbsolut; ?>&Subject=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informações%20-%20<?php echo $nome_empresa; ?>">
                        <i class="fas fa-envelope-open-text"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span>
                    </a> 
                    
                </div>
            </div>
            <div class="col-md-2">
                <div class="redes-topo">
                    <a href="" target="_blank" title="Facebook <?php echo $nome_empresa; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a href="" target="_blank" title="Instagram <?php echo $nome_empresa; ?>"><i class="fab fa-instagram"></i></a>
                    <a href="" target="_blank" title="Youtube <?php echo $nome_empresa; ?>"><i class="fab fa-youtube"></i></a>
                    <a href="" target="_blank" title="Linkedin <?php echo $nome_empresa; ?>"><i class="fab fa-linkedin-in"></i></a> 
                </div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="col-md-3">
            <div class="logo">
                <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                    <span itemprop="image">
                        <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                    </span>
                </a>
            </div>
        </div>
        
        <div class="col-md-9">
            <nav class="menu">
                <ul class="menu-list">
                    <li><a href="https://apliflow.com.br/" title="Página inicial">Home</a></li>
                    <li><a href="https://apliflow.com.br/representadas/" title="Representadas">Representadas</a></li>
                    <li><a href="https://apliflow.com.br/produtos/" title="Produtos">Produtos</a></li>
                    <li><a href="https://apliflow.com.br/servicos/" title="Serviços">Serviços</a></li>
                    <li><a href="https://apliflow.com.br/manuais/" title="Manuais">Manuais</a></li>
                    <li><a href="<?php echo $url; ?>" title="Informações">Informações</a></li>
                    <li><a href="https://apliflow.com.br/area-do-cliente-2/" title="Área do Cliente">Área do Cliente</a></li>
                    
                </ul>
            </nav>
        </div>
    </div>
</header>