<footer>
    <?php include "includes/btn-fixos.php"; ?>
    
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p class="desenvolvido">Copyright © 2021 <?php echo $nome_empresa; ?></p>
                </div>
                <div class="col-md-4 text-center">
                    <div class="baixa-absolut"></div>
                    <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" target="_blank" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                </div>
                <div class="col-md-4 text-right">
                    <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                        <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a href="whatsapp://send?text=<?php echo $canonical; ?>" class="mm-whatsapp" title="Whats App"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
    </footer>
    <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-N9NX2T4FSW"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-N9NX2T4FSW');
      </script>


      <?php } ?>